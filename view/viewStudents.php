<form method="get">
    <div class="form-content">
        <div class="formtop">
            <div class="selector">
                <select name="promotion">
                    <option value="">-- Promotion --</option>
                    <?php foreach ($data_promo as $promo) {
                        $selected;
                        if ($_GET['promotion'] == $promo->id) {
                            $selected = 'selected=true';
                        } else {
                            $selected = '';
                        }
                        echo '<option ' . $selected . 'value=' . $promo->id . '>' . $promo->name . '</option>';
                    } ?>
                </select>
            </div>
            <div class="researchor">
                <?php
                $valora = "";
                if (isset($_GET['search'])) {
                    $valora = "value='" . $_GET['search'] . "'";
                }
                echo '<input ' . $valora . ' type="text" name="search" id="search" placeholder="Rechercher...">';
                ?>
            </div>
            <div class="checkor">
                <select name="skill" class="selectorskill">
                    <option value=""> -- Compétences --</option>
                    <?php foreach ($data_skill as $skill) {
                        $selected;
                        if ($_GET['skill'] == $skill->id) {
                            $selected = 'selected=true';
                        } else {
                            $selected = '';
                        }
                        echo '<option ' . $selected . 'value=' . $skill->id . '>' . $skill->name . '</option>';
                    } ?>
                </select>
            </div>
        </div>
        <div class="divbot">
            <input type="submit" value="ENVOYER">
        </div>
    </div>
</form>

<div class="students container">
    <?php foreach ($data_student as $student) : ?>
        <div class='card'>
            <div class='card-content'>
                <div class='card-content-header'>
                    <img src='<?= $student->image ?>' />
                </div>
                <div class='card-content-content'>
                    <div>
                        <h2><?= $student->title->rendered ?></h2>
                        <h3><?= $student->promotion->name ?></h2>
                            <?php
                            foreach ($student->competences as $competence) {
                                echo '<p class="skill">' . $competence->name . '</span>';
                            }
                            ?>
                    </div>
                </div>
            </div>
            <div class='card-filter'>
                <div class='card-filter-header'>
                    <h2><?= $student->title->rendered ?></h2>
                </div>
                <div class='card-filter-content'>
                    <p><?= $student->excerpt->rendered ?></p>
                </div>
                <div class='card-filter-footer'>
                    <a target="_blank" href="<?= $student->linkedin ?>">LinkedIn</a>
                    <a target="_blank" href="<?= $student->cv ?>">CV</a>
                    <a target="_blank" href="<?= $student->porfolio ?>">Portfolio</a>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>