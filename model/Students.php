<?php

class Students
{
    public $apiC26;

    public function __construct($apiC26)
    {
        $this->apiC26 = $apiC26;
    }

    public function getApprenticeData($arg)
    {
        $data = $this->fetchData("apprenant",$arg);
        return $this->decode($data);
    }

    public function getPromoData()
    {
        $data = $this->fetchData("formation");
        return $this->decode($data);
    }

    public function getSkillsData()
    {
        $data = $this->fetchData("competence");
        return $this->decode($data);
    }

    private function fetchData($route, $arg=null)
    {
        switch($route){
            case "apprenant": 
                $param = "apprenants?_fields=id,title,excerpt,portfolio,linkedin,cv,image,competences,promotion&per_page=100";
                if($arg!=null){
                    if(!empty($arg['promotion'])){
                        $param .='&Promotion=' . $arg['promotion'];
                    }
                    if(!empty($arg['search'])){
                        $param .='&search=' . $arg['search'];
                    }
                    if(!empty($arg['skill'])){
                        $param .='&Competence=' . $arg['skill'];
                    }
                }
            break;
            case "formation": $param = "promotion?_fields=id,name,count";
            break;
            case "competence": $param = "competence?_fields=id,name,count";
            break;

        }
        
        $curl = curl_init($this->apiC26 . $param);
        // $this->apiC26."/apprenants?_fields=id,title,excerpt,Promotion,Competence,portfolio,linkedin,cv,image";
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($curl);
        if (!$data) {
            throw new Error("Erreur ! L'API est introuvable (n'est pas un code 200) !");
        }

        return $data;
    }

    

    private function decode($jsonString)
    {
        return json_decode($jsonString);
    }
}
