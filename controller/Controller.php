<?php
class Controller
{
    private $templateEngineDirectory;
    private $request;

    public function __construct($templateEngineDirectory, $request)
    {
        $this->templateEngineDirectory = $templateEngineDirectory;
        $this->request = $request;
    }

    public function displayList()
    {
        $data_student = $this->request->getApprenticeData($_GET);
        $data_skill = $this->request->getSkillsData();
        $data_promo = $this->request->getPromoData();
        return $this->templateEngineDirectory->render('viewStudents.php', 
        array(
            'data_student' => $data_student, 
            'data_skill' => $data_skill,
            'data_promo' => $data_promo
        ));
    }

    public function displayPart($part)
    {
        $part_template = $this->templateEngineDirectory;
        return $part_template->render($part . '.php');
    }
}
