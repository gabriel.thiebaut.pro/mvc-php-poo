<?php

require_once 'controller/Controller.php';
require_once 'app/TemplateEngine.php';
require_once 'model/Students.php';

$urlApi = "https://www.lecoledunumerique.fr/wp-json/wp/v2/";
$templateEngineDirectory = new TemplateEngine('view');
$request = new Students($urlApi);
$controller = new Controller($templateEngineDirectory, $request);

echo $controller->displayPart('viewHeader');
echo $controller->displayList();
// echo $controller->displayPart('viewFooter');
